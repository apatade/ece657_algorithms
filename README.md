<h1>Optimization of Desicion Tree Algorithm </h1>

Since the term “Artificial Intelligence” was coined in 1950’s - Machine Learning has been
under the limelight of researchers. A significant reason is that intelligent systems lead to high
performance and have a variety of applications. This research project explicitly focuses on
classification algorithms for medical symptom diagnosis. Decision tree-based algorithms have
numerous applications in diagnosis of breast cancer, diabetes (type 2), and liver disorders [1],
[2]. However, identifying the algorithm based on its accuracy and other parameters for the
domain specific need has been challenging. Classification algorithms have evolved over the
time and it has been increasingly difficult to choose one for special purpose. From the early
works of Quinlan in ID3 to significant developments to handle missing data in C 4.5; to the
recent algorithms that use hardware-software co-design for optimization: these algorithms have
improvised considerably [3], [4], [5].
Our clear aim is to study the current state-of-the-art algorithms and attempt to improve one or
more parameters among performance, information gain, memory usage, scale, complexity,
speed and accuracy as per our discovered requirements. We propose to implement the
algorithms from scratch and validate their results. The tool we are supposed to use is the WEKA
(Waikato Environment for Knowledge Analysis) tool which is ominously used for data preprocessing
and data mining [6]. The datasets we will be using are the Breast Cancer Wisconsin
dataset, Diabetes dataset and even aspire to use other datasets and intelligent system tools to
do justice to the research [7], [8]. Moreover, after implementing our own version of such
algorithms and studying their shortcomings we intend to improvise and optimize them. We are
willing to bring a novel improvised algorithm on the table that suits the best to our domain
requirements and provide a comparative analysis with the previous versions. Finally, after
thorough research, producing results and comprehensive documentation, we target to
contribute a well-structured, detailed, original research paper.

References:
[1] A. Al Jarullah, "Decision tree discovery for the diagnosis of type II diabetes,"
International Conference on Innovations in Information Technology, Abu Dhabi, 2011,
pp. 303-307,
[2] Chin-YuanFan et. al. “A hybrid model combining case-based reasoning and fuzzy
decision tree for medical data classification” Applied Soft Computing Volume 11, Issue
1, January 2011, Pages 632-644
[3] Quinlan, J. R. 1986,” Induction of Decision Trees. Mach. Learn” 1, 1 (Mar. 1986), 81–
106
[4] J. R. Quinlan. “Improved use of continuous attributes in c4.5.” Journal of Artificial
Intelligence Research, 4:77-90, 1996.
[5] B. Vukobratović and R. Struharik, "Hardware acceleration of non-incremental
algorithms for the induction of decision trees," 2017 25th Telecommunication Forum
(TELFOR), Belgrade, 2017, pp. 1-8, doi: 10.1109/TELFOR.2017.8249396.
[6] Witten, Ian H.; Frank, Eibe; Trigg, Len; et. al. (1999). "Weka: Practical Machine
Learning Tools and Techniques with Java Implementations" (PDF). Proceedings of the
ICONIP/ANZIIS/ANNES'99 Workshop on Emerging Knowledge Engineering and
Connectionist-Based Information Systems. pp. 192–196.
[7] Michael Kahn, Washington University, St. Louis, MO UCI Machine Learning
Repository [http://archive.ics.uci.edu/ml]. Irvine, CA: University of California, School
of Information and Computer Science
[8] Dr. William H. Wolberg, General Surgery Dept. University of Wisconsin et. al., UCI
Machine Learning Repository [http://archive.ics.uci.edu/ml]. Irvine, CA: University of
California, School of Information and Computer Science